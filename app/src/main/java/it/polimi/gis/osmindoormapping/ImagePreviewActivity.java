package it.polimi.gis.osmindoormapping;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class ImagePreviewActivity extends AppCompatActivity {

    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        imageUri = getIntent().getParcelableExtra("ImageUri");

        ImageView view = (ImageView) findViewById(R.id.imagePreview);

        if (imageUri != null) {

            Point screenSize = new Point();
            getWindowManager().getDefaultDisplay().getSize(screenSize);
            Bitmap image = ImageProcessor.findOptimalSize(imageUri.getPath(),screenSize.x);

            if (view != null) {
                view.setAdjustViewBounds(true);
                view.setImageBitmap(image);
            }
        }
    }

    public void startGeoreferenceActivity(View view) {
        Intent sendImage = new Intent(this, GeoreferenceActivity.class);
        sendImage.putExtra("ImageUri", imageUri);
        this.startActivity(sendImage);
    }

    public void startIndoorMappingActivity(View view) {
        Intent sendImage = new Intent(this, ImageActivity.class);
        sendImage.putExtra("ImageUri", imageUri);
        this.startActivity(sendImage);
    }

    public void startPerspectiveActivity(View view) {
        Intent sendImage = new Intent(this, PerspectiveActivity.class);
        sendImage.putExtra("ImageUri", imageUri);
        this.startActivity(sendImage);
    }
}
