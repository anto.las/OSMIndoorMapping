package it.polimi.gis.osmindoormapping;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;

public class PerspectiveActivity extends AppCompatActivity {

    private Point previewImageSize;
    private Point realImageSize;
    private Uri imageUri;
    private RadioButton oldBt;
    private String newTag = "";
    private Point[] corners = new Point[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perspective);

        imageUri = getIntent().getParcelableExtra("ImageUri");

        loadImage();
    }

    private void loadImage() {

        ImageView view = (ImageView) findViewById(R.id.imageToCorrectPerspective);

        if (imageUri != null) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageUri.getPath(), options);
            realImageSize = new Point(options.outWidth, options.outHeight);
            System.out.println("Real Image size. width: " + realImageSize.x + ",height: " + realImageSize.y);

            android.graphics.Point screenSize = new android.graphics.Point();
            getWindowManager().getDefaultDisplay().getSize(screenSize);
            Bitmap image = ImageProcessor.findOptimalSize(imageUri.getPath(), screenSize.x);

            if (view != null && image != null) {
                view.setImageBitmap(image);
                view.setAdjustViewBounds(true);

                previewImageSize = new Point(image.getWidth(), image.getHeight());
                System.out.println("\nPreview Image size. width: " + previewImageSize.x + ",height: " + previewImageSize.y);

                view.setOnTouchListener(onTouchListener);
            }
        }
    }


    public void originCoordSetting(View view) {
        if (oldBt != null) oldBt.setChecked(false);
        ((RadioButton) view).setChecked(true);
        newTag = view.getTag().toString();
        oldBt = ((RadioButton) view);
    }

    View.OnTouchListener onTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            Point viewSize = new Point(v.getWidth(), v.getHeight());
            System.out.println("\nView size. width: " + viewSize.x + ",height: " + viewSize.y);

            Point relativeCoordinates = new Point(x, y);
            System.out.println("\nRelative coordinates. x: " + relativeCoordinates.x + ",y: " + relativeCoordinates.y);

            Point realCoordinates = ImageProcessor.getRealCoordinates(relativeCoordinates, viewSize, previewImageSize, realImageSize);
            System.out.println("Real coordinates. x: " + realCoordinates.x + ",y: " + realCoordinates.y);

            updatePoints(realCoordinates, relativeCoordinates);
            return true;
        }
    };

    private void updatePoints(Point realCoordinates, Point relativeCoordinates) {
        EditText etX = null;
        EditText etY = null;

        switch (newTag) {
            case "0":
                etX = (EditText) findViewById(R.id.tlX);
                etY = (EditText) findViewById(R.id.tlY);
                corners[0] = realCoordinates.clone();
                break;
            case "1":
                etX = (EditText) findViewById(R.id.trX);
                etY = (EditText) findViewById(R.id.trY);
                corners[1] = realCoordinates.clone();
                break;
            case "2":
                etX = (EditText) findViewById(R.id.blX);
                etY = (EditText) findViewById(R.id.blY);
                corners[2] = realCoordinates.clone();
                break;
            case "3":
                etX = (EditText) findViewById(R.id.brX);
                etY = (EditText) findViewById(R.id.brY);
                corners[3] = realCoordinates.clone();
                break;
        }

        if (!newTag.equals("")) {

            ImageView c;
            c = new ImageView(this);
            if (findViewById(Integer.parseInt(newTag)) == null) {

                c.setId(Integer.parseInt(newTag));
                c.setImageDrawable(getResources().getDrawable(R.drawable.circlefull));
                c.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                final float scale = getResources().getDisplayMetrics().density;
                c.getLayoutParams().width = (int) (10 * scale);
                c.getLayoutParams().height = (int) (10 * scale);
                c.setX((int) relativeCoordinates.x - (int) (10 * scale) / 2);
                c.setY((int) relativeCoordinates.y - (int) (10 * scale) / 2);

                ViewGroup main = (ViewGroup) findViewById(R.id.mainConteiner);
                if (main != null) {
                    main.addView(c);
                }
            } else {
                c = (ImageView) findViewById(Integer.parseInt(newTag));
                c.setX((int) relativeCoordinates.x - c.getWidth() / 2);
                c.setY((int) relativeCoordinates.y - c.getHeight() / 2);
            }
            if (etX != null) {
                etX.setText(Integer.toString((int) realCoordinates.x));
            }
            if (etY != null) {
                etY.setText(Integer.toString((int) realCoordinates.y));
            }

        }
    }


    public void startPerspectiveCorrection(View view) {

        if (corners[0] == null || corners[1] == null || corners[2] == null || corners[3] == null) {

            Toast.makeText(getApplicationContext(), "All corners need to be selected.", Toast.LENGTH_LONG).show();

        } else {

            Mat srcImage = Imgcodecs.imread(imageUri.getPath());
            Mat dstImage = ImageProcessor.adjustPerspective(srcImage, corners);

            Imgcodecs.imwrite(imageUri.getPath(), dstImage);

            loadImage();
        }
    }
}
