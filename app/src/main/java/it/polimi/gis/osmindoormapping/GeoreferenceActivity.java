package it.polimi.gis.osmindoormapping;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;

public class GeoreferenceActivity extends AppCompatActivity {

    private Point previewImageSize;
    private Point realImageSize;
    private Point viewSize;
    private Uri imageUri;
    private RadioButton oldBt;
    private String newTag = "";

    private Point[] srcPoints = new Point[3];
    private Point[] dstPoints = new Point[3];
    private Point origin = null;
    private int alfa = 0;
    private Point point1ref = null;
    private Point point2ref = null;
    private double meters = 0;
    private double ratioPixelMeters = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_georeference);

        imageUri = getIntent().getParcelableExtra("ImageUri");

        loadImage();

    }

    private void loadImage() {
        ImageView view = (ImageView) findViewById(R.id.imageToGeoreference);

        if (imageUri != null) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageUri.getPath(), options);
            realImageSize = new Point(options.outWidth, options.outHeight);

            android.graphics.Point screenSize = new android.graphics.Point();
            getWindowManager().getDefaultDisplay().getSize(screenSize);
            Bitmap image = ImageProcessor.findOptimalSize(imageUri.getPath(), screenSize.x);

            if (view != null && image != null) {
                view.setImageBitmap(image);
                view.setAdjustViewBounds(true);

                previewImageSize = new Point(image.getWidth(), image.getHeight());

                view.setOnTouchListener(onTouchListener);
            }
        }
    }

    public void originCoordSetting(View view) {
        if (oldBt != null) oldBt.setChecked(false);
        ((RadioButton) view).setChecked(true);
        newTag = view.getTag().toString();
        oldBt = ((RadioButton) view);
    }

    View.OnTouchListener onTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            viewSize = new Point(v.getWidth(), v.getHeight());

            Point relativeCoordinates = new Point(x, y);

            Point realCoordinates = ImageProcessor.getRealCoordinates(relativeCoordinates, viewSize, previewImageSize, realImageSize);

            updateCoordinatesFromPoint(realCoordinates, relativeCoordinates);
            return true;
        }
    };

    //Update using GRAPHIC position values: update MODEL and EDIT TEXT
    private void updateCoordinatesFromPoint(Point realCoordinates, Point relativeCoordinates) {
        EditText etX = null;
        EditText etY = null;
        View c = null;

        switch (newTag) {
            case "0":
                etX = (EditText) findViewById(R.id.coordX);
                etY = (EditText) findViewById(R.id.coordY);
                EditText etalfa = (EditText) findViewById(R.id.alfaX);
                if (etalfa != null) {
                    etalfa.setText(Integer.toString(alfa));
                }

                origin = realCoordinates.clone();

                if (findViewById(R.id.xAssix) == null) {

                    ImageView assixX = new ImageView(this);

                    assixX.setId(R.id.xAssix);
                    assixX.setImageDrawable(getResources().getDrawable(R.drawable.cartesian));
                    assixX.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    final float scale = getResources().getDisplayMetrics().density;
                    assixX.getLayoutParams().width = (int) (200 * scale);
                    assixX.getLayoutParams().height = (int) (200 * scale);

                    ViewGroup main = (ViewGroup) findViewById(R.id.imageSection);
                    if (main != null) {
                        main.addView(assixX);
                    }

                    assixX.setX((int) relativeCoordinates.x - (int) (200 * scale / 2));
                    assixX.setY((int) relativeCoordinates.y - (int) (200 * scale / 2));
                } else {

                    ImageView assixX = (ImageView) findViewById(R.id.xAssix);
                    assixX.setX((int) relativeCoordinates.x - assixX.getWidth() / 2);
                    assixX.setY((int) relativeCoordinates.y - assixX.getHeight() / 2);
                }

                break;
            case "1":
                if (((SwitchCompat) findViewById(R.id.switch1)).isChecked()) {
                    c = findViewById(R.id.circlef1);
                    etX = (EditText) findViewById(R.id.newX1);
                    etY = (EditText) findViewById(R.id.newY1);

                    dstPoints[0] = realCoordinates.clone();
                } else {
                    c = findViewById(R.id.circlee1);
                    etX = (EditText) findViewById(R.id.oldX1);
                    etY = (EditText) findViewById(R.id.oldY1);

                    srcPoints[0] = realCoordinates.clone();
                }
                break;
            case "2":
                if (((SwitchCompat) findViewById(R.id.switch2)).isChecked()) {
                    c = findViewById(R.id.circlef2);
                    etX = (EditText) findViewById(R.id.newX2);
                    etY = (EditText) findViewById(R.id.newY2);

                    dstPoints[1] = realCoordinates.clone();
                } else {
                    c = findViewById(R.id.circlee2);
                    etX = (EditText) findViewById(R.id.oldX2);
                    etY = (EditText) findViewById(R.id.oldY2);

                    srcPoints[1] = realCoordinates.clone();
                }
                break;
            case "3":
                if (((SwitchCompat) findViewById(R.id.switch3)).isChecked()) {
                    c = findViewById(R.id.circlef3);
                    etX = (EditText) findViewById(R.id.newX3);
                    etY = (EditText) findViewById(R.id.newY3);

                    dstPoints[2] = realCoordinates.clone();
                } else {
                    c = findViewById(R.id.circlee3);
                    etX = (EditText) findViewById(R.id.oldX3);
                    etY = (EditText) findViewById(R.id.oldY3);

                    srcPoints[2] = realCoordinates.clone();
                }
                break;
            case "4":
                c = findViewById(R.id.circlee4);
                etX = (EditText) findViewById(R.id.x1ref);
                etY = (EditText) findViewById(R.id.y1ref);

                point1ref = realCoordinates.clone();

                break;
            case "5":
                c = findViewById(R.id.circlee5);
                etX = (EditText) findViewById(R.id.x2ref);
                etY = (EditText) findViewById(R.id.y2ref);

                point2ref = realCoordinates.clone();

                break;
        }

        Point coordinatesToShow;
        if (!newTag.equals("0") && !newTag.equals("5") && !newTag.equals("4")) {
            coordinatesToShow = reprojectToLocalCoordinates(realCoordinates);
        } else {
            coordinatesToShow = realCoordinates;
        }

        if (etX != null) {
            etX.setText(Integer.toString((int) coordinatesToShow.x));
        }
        if (etY != null) {
            etY.setText(Integer.toString((int) coordinatesToShow.y));
        }

        if (c != null) {
            c.setVisibility(View.VISIBLE);
            c.setX((int) relativeCoordinates.x - c.getWidth() / 2);
            c.setY((int) relativeCoordinates.y - c.getHeight() / 2);
        }
    }

    //Update using EDIT TEXT values: update MODEL and GRAPHICS
    public void updateCoordinatesFromText(View view) {

        Point realCoordinates = new Point();
        EditText etx = null;
        EditText ety = null;
        View point = null;
        boolean reprojectFromLocal = false;

        int x;
        int y;

        switch (view.getId()) {
            case R.id.point1button:
                if (((SwitchCompat) findViewById(R.id.switch1)).isChecked()) {
                    etx = (EditText) findViewById(R.id.newX1);
                    ety = (EditText) findViewById(R.id.newY1);
                    point = findViewById(R.id.circlef1);
                } else {
                    etx = (EditText) findViewById(R.id.oldX1);
                    ety = (EditText) findViewById(R.id.oldY1);
                    point = findViewById(R.id.circlee1);
                }
                reprojectFromLocal = true;
                break;

            case R.id.point2button:
                if (((SwitchCompat) findViewById(R.id.switch2)).isChecked()) {
                    etx = (EditText) findViewById(R.id.newX2);
                    ety = (EditText) findViewById(R.id.newY2);
                    point = findViewById(R.id.circlef2);
                } else {
                    etx = (EditText) findViewById(R.id.oldX2);
                    ety = (EditText) findViewById(R.id.oldY2);
                    point = findViewById(R.id.circlee2);
                }
                reprojectFromLocal = true;
                break;

            case R.id.point3button:
                if (((SwitchCompat) findViewById(R.id.switch3)).isChecked()) {
                    etx = (EditText) findViewById(R.id.newX3);
                    ety = (EditText) findViewById(R.id.newY3);
                    point = findViewById(R.id.circlef3);
                } else {
                    etx = (EditText) findViewById(R.id.oldX3);
                    ety = (EditText) findViewById(R.id.oldY3);
                    point = findViewById(R.id.circlee3);
                }
                reprojectFromLocal = true;
                break;

            case R.id.point4button:
                etx = (EditText) findViewById(R.id.x1ref);
                ety = (EditText) findViewById(R.id.y1ref);
                point = findViewById(R.id.circlee4);

                x = Integer.parseInt(etx.getText().toString());
                y = Integer.parseInt(ety.getText().toString());

                realCoordinates.x = x;
                realCoordinates.y = y;

                point1ref = realCoordinates.clone();

                break;

            case R.id.point5button:
                etx = (EditText) findViewById(R.id.x2ref);
                ety = (EditText) findViewById(R.id.y2ref);
                point = findViewById(R.id.circlee5);

                x = Integer.parseInt(etx.getText().toString());
                y = Integer.parseInt(ety.getText().toString());

                realCoordinates.x = x;
                realCoordinates.y = y;

                point2ref = realCoordinates.clone();

                break;
        }

        try {

            System.out.println(etx.getText().toString());
            System.out.println(ety.getText().toString());

            if (reprojectFromLocal) {

                x = Integer.parseInt(etx.getText().toString());
                y = Integer.parseInt(ety.getText().toString());
                realCoordinates = reprojectFromLocalCoordinates(new Point(x, y));
            }

            Point relativeCoordinates = ImageProcessor.getRelativeCoordinates(realCoordinates, viewSize, previewImageSize, realImageSize);

            point.setX((int) relativeCoordinates.x - point.getWidth() / 2);
            point.setY((int) relativeCoordinates.y - point.getHeight() / 2);

        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Insert correct coordinates.", Toast.LENGTH_LONG).show();
        }
    }

    //Update using EDIT TEXT values: update MODEL and GRAPHICS
    public void updateSystemCoordinates(View view) {

        Point realCoordinates = new Point();
        EditText etx = (EditText) findViewById(R.id.coordX);
        EditText ety = (EditText) findViewById(R.id.coordY);
        EditText alfatext = (EditText) findViewById(R.id.alfaX);

        try {

            //Retrieve values
            int x = Integer.parseInt(etx.getText().toString());
            int y = Integer.parseInt(ety.getText().toString());
            alfa = Integer.parseInt(alfatext.getText().toString());

            realCoordinates.x = x;
            realCoordinates.y = y;

            //Update MODEL
            origin = realCoordinates.clone();

            //Compute the "onScreen" coordinates
            Point relativeCoordinates = ImageProcessor.getRelativeCoordinates(realCoordinates, viewSize, previewImageSize, realImageSize);

            //Update GRAPHICS
            ImageView assixX = (ImageView) findViewById(R.id.xAssix);
            assixX.setX((int) relativeCoordinates.x - assixX.getWidth() / 2);
            assixX.setY((int) relativeCoordinates.y - assixX.getHeight() / 2);
            assixX.setRotation(-alfa);

        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Insert correct coordinates.", Toast.LENGTH_LONG).show();
        }
    }

    public void saveSystemCoordinates(View view) {

        EditText metersText = (EditText) findViewById(R.id.metersref);
        try {

            meters = Double.parseDouble(metersText.getText().toString());

        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Insert correct coordinates.", Toast.LENGTH_LONG).show();
        }

        if (origin == null || point1ref == null || point2ref == null || meters == 0) {
            Toast.makeText(getApplicationContext(), "Insert correct coordinates.", Toast.LENGTH_LONG).show();
        } else {

            double dRef = Math.sqrt(Math.pow(point2ref.x - point1ref.x, 2) + Math.pow(point2ref.y - point1ref.y, 2));
            ratioPixelMeters = meters / dRef;

            View layout = findViewById(R.id.coordinatescontainer);
            if (layout != null) {
                layout.setVisibility(View.GONE);
            }
            layout = findViewById(R.id.gcpscontainer);
            if (layout != null) {
                layout.setVisibility(View.VISIBLE);
            }
            newTag = "";

            View c = findViewById(R.id.circlee4);
            if (c != null) {
                c.setVisibility(View.GONE);
            }
            c = findViewById(R.id.circlee5);
            if (c != null) {
                c.setVisibility(View.GONE);
            }
        }
    }

    public Point reprojectToLocalCoordinates(Point coordinates) {
        if (origin != null) {

            double alfaRad = Math.toRadians(alfa);

            double xTransl = coordinates.x - origin.x;
            double yTransl = coordinates.y - origin.y;

            double xRot = xTransl * Math.cos(alfaRad) + yTransl * Math.sin(alfaRad);
            double yRot = -xTransl * Math.sin(alfaRad) + yTransl * Math.cos(alfaRad);

            double xInMeters = xRot * ratioPixelMeters;
            double yInMeters = yRot * ratioPixelMeters;

            return new Point(xInMeters, yInMeters);
        } else {
            return coordinates;
        }
    }

    public Point reprojectFromLocalCoordinates(Point coordinates) {
        if (origin != null) {

            double alfaRad = Math.toRadians(alfa);

            double xInPixel = coordinates.x / ratioPixelMeters;
            double yInPixel = coordinates.y / ratioPixelMeters;

            double xRot = xInPixel * Math.cos(alfaRad) - yInPixel * Math.sin(alfaRad);
            double yRot = xInPixel * Math.sin(alfaRad) + yInPixel * Math.cos(alfaRad);

            double xTransl = xRot + origin.x;
            double yTransl = yRot + origin.y;

            return new Point(xTransl, yTransl);
        } else {
            return coordinates;
        }
    }

    public void startGeoreference(View view) {

        boolean notComplete = false;
        for (int i = 0; i < srcPoints.length; i++) {
            if (srcPoints[i] == null)
                notComplete = true;
        }
        for (int i = 0; i < dstPoints.length; i++) {
            if (srcPoints[i] == null)
                notComplete = true;
        }

        if (notComplete) {
            Toast.makeText(getApplicationContext(), "All points need to be selected.", Toast.LENGTH_LONG).show();
        } else {
            Mat srcImage = Imgcodecs.imread(imageUri.getPath());
            Mat dstImage = ImageProcessor.remapCoordinate(srcImage, srcPoints, dstPoints);

            Imgcodecs.imwrite(imageUri.getPath(), dstImage);

            Toast.makeText(getApplicationContext(), "Image Georeferenced.", Toast.LENGTH_LONG).show();

            View c = findViewById(R.id.circlee1);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlee2);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlee3);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlef0);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlef1);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlef2);
            c.setVisibility(View.GONE);
            c = findViewById(R.id.circlef3);
            c.setVisibility(View.GONE);

            loadImage();
        }
    }
}
