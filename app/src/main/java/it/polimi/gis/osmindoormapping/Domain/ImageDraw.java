package it.polimi.gis.osmindoormapping.Domain;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImageDraw extends ImageView {
    List<Point> pts = new ArrayList<Point>();
    Path wallpath = new Path();
    Paint paint = new  Paint();
    Point meanp;
    boolean shouldClick = false;
    int check;
    private String name = "";
    Canvas myCanvas = new Canvas();
    private boolean isNotBuild = true;

    public ImageDraw(Context context) {
        super(context);

        wallpath.reset();


    }

    //used to send the location of the points to draw on the screen
//must be called before every redraw to update the points on the screen
    public void SetPointsToDraw(List<Point> pts) {
        this.pts = pts;
    }


    public ImageDraw(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageDraw(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    private float startX;
    private float startY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                shouldClick = true;
                break;
            case MotionEvent.ACTION_UP:
                if (shouldClick) {
                    drawPoint((int) event.getX(), (int) event.getY());
                    postInvalidate();

                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                //Do your stuff
                break;
        }
        return true; // processed
    }

    public void drawPoint(int x, int y) {
        pts.add(new Point(x, y));
        this.draw(myCanvas);
    }

    private static boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        if (differenceX > 5 || differenceY > 5) {
            return false;
        }
        return true;
    }

    public boolean isNotBuild() {
        return isNotBuild;
    }

    public void setNotBuild(boolean notBuild) {
        isNotBuild = notBuild;
    }

    public void saveCanvas() {
        //  myCanvas.drawColor(0, PorterDuff.Mode.CLEAR);

        check = 1;

        meanp = new Point();
        // only needed when reusing this path for a new build

        wallpath.moveTo(pts.get(0).x, pts.get(0).y);
        meanp.x = pts.get(0).x;
        meanp.y = pts.get(0).y;
        for (int i = 1; i < pts.size(); i++) {

            meanp.x += pts.get(i).x;
            meanp.y += pts.get(i).y;
            wallpath.lineTo(pts.get(i).x, pts.get(i).y);
        }

        meanp.x /= pts.size();
        meanp.y /= pts.size();
        //  wallpath.moveTo(pts.get(pts.size() - 1).x, pts.get(pts.size() - 1).y);
        wallpath.lineTo(pts.get(0).x, pts.get(0).y);
        wallpath.close();

        if(isNotBuild == false) {
            paint = new Paint();
            paint.setColor(Color.DKGRAY);
            paint.setStrokeWidth(5);
            paint.setStyle(Paint.Style.STROKE);
            paint.setPathEffect(new DashPathEffect(new float[]{5, 5}, 5));
        }
        else{

            paint = new Paint();
            paint.setStrokeWidth(3);
            Random rnd = new Random();
            paint.setARGB(50, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setAntiAlias(true);
          }
        this.draw(myCanvas);
        postInvalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (check == 0) {

            if (pts.size() > 0) {
                paint.setStrokeWidth(3);
                paint.setColor(Color.YELLOW);
                canvas.drawCircle(pts.get(0).x, pts.get(0).y, 7, paint);
            }
            if (pts.size() > 1) {
                for (int i = 1; i < pts.size(); i++) {

                    paint.setStrokeWidth(3);
                    paint.setColor(Color.RED);
                    canvas.drawLine(pts.get(i - 1).x, pts.get(i - 1).y, pts.get(i).x, pts.get(i).y, paint);
                }
            }
        } else {

            if (isNotBuild) {
                canvas.drawPath(wallpath, paint);
                if(name != "") {
                   Paint  p = new Paint();
                    p.setColor(Color.BLACK);
                    p.setTextSize(48);
                    p.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(name, meanp.x, meanp.y, p);
                }
            } else {

                canvas.drawPath(wallpath, paint);
            }

        }
    }

    public List<Point> getPoints() {

        List<Point> pts2 = new ArrayList<>(pts);

        return pts2;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void Clear() {
        pts = new ArrayList<Point>();
        wallpath = new Path();
        paint =new Paint();
        meanp = new Point();
        check = 0;
        String name = "";
        myCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        postInvalidate();
    }

    public void setPath( List<Point> path) {
        this.pts = path;
    }
}