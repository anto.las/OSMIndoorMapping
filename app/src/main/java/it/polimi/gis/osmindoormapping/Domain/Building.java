package it.polimi.gis.osmindoormapping.Domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Claudio on 21/06/2016.
 */
public class Building {
    public Building() {}

    public enum BuildCondition {
        EMPTY(""),
        EXCELLENT("Excellent"),
        GOOD("Good"),
        BAD("Bad"),
        CRUMBLING("Crumbling");

        private String theState;

        BuildCondition(String aState) {
            theState = aState;
        }

        @Override public String toString() {
            return theState;}
    };




    public String getArchitetture() {
        return architetture;
    }

    public void setArchitetture(String architetture) {
        this.architetture = architetture;
    }

    public String getBuildYear()
    {
        if(buildYear != Short.MIN_VALUE)    return Short.toString(buildYear);
        else return null;
    }

    public void setBuildYear(String buildYear) {
        if(buildYear.isEmpty() || buildYear == null || buildYear == "")  this.buildYear = 0;
        else this.buildYear =Short.parseShort(buildYear);
    }

    public String getCladding() {
        return cladding;
    }

    public void setCladding(String cladding) {
        this.cladding = cladding;
    }

    public BuildCondition getCondition() {
        return condition;
    }

    public void setCondition(BuildCondition condition) {
        this.condition = condition;
    }

    public String getArchitect() {
        return architect;
    }

    public void setArchitect(String architect) {
        this.architect = architect;
    }

    public Roof getRoof() {
        return roof;
    }

    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public Facade getFacade() {
        return facade;
    }

    public void setFacade(Facade facade) {
        this.facade = facade;
    }

    public String getHeight()
    {
        if(height != Float.MIN_VALUE)  return Float.toString(height);
        else return null;

    }

    public void setHeight(String height) {
        if(height.isEmpty() || height == null || height == "")   this.height = 0f;
        else  this.height = Float.parseFloat(height);
    }


    public String getLevels() {
        if(levels != Short.MIN_VALUE)    return Short.toString(levels);
        else return null;
    }

    public void setLevels(String levels) {
        if(levels.isEmpty() || levels == null || levels == "")  this.levels = 0;
        else this.levels = Short.parseShort(levels);
    }


    public Element getBase() {
        return base;
    }

    public void setBase(Element base) {
        this.base = base;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    private Element base = new Element();
    private List<Element> elements = new ArrayList();
    private String architetture = "";
    private short buildYear =  Short.MIN_VALUE;
    private String cladding="";
    private BuildCondition condition;
    private String architect ="";
    private Roof roof = new Roof();
    private Facade facade = new Facade();
    private float height = Float.MIN_VALUE;
    //private String name;
    private short levels = Short.MIN_VALUE;
    private short min_level;
    private short max_level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name= "";



}

