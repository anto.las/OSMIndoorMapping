package it.polimi.gis.osmindoormapping.Domain;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Claudio on 21/06/2016.
 */
public class BuildingPart {

    private List<Element> elements = new ArrayList();
    public enum BuildPartType {
        EMPTY(""),
        ROOM("room"),
        AREA("area"),
        WALL("wall"),
        CORRIDOR("corridor");

        private String theState;

        BuildPartType(String aState) {
            theState = aState;
        }

        @Override public String toString() {
            return theState;}
        };

    private Element base = new Element();
    private BuildPartType builtpartType;
    private String isWindow;
    private float windowWidth = Float.MIN_VALUE;
    private float windowHeight = Float.MIN_VALUE;
    private float windowBreast = Float.MIN_VALUE;
    private float height = Float.MIN_VALUE;
    private String name;
    private String isDoor;
    private float doorWidth = Float.MIN_VALUE;
    private float doorHeight = Float.MIN_VALUE;

    public BuildPartType getBuiltpartType() {
        return builtpartType;
    }

    public void setBuiltpartType(BuildPartType builtpartType) {
        this.builtpartType = builtpartType;
    }

    public Element getBase() {
        return base;
    }

    public void setBase(Element base) {
        this.base = base;
    }

    public String getDoorWidth() {
        if(doorWidth != Float.MIN_VALUE)    return Float.toString(doorWidth);
        else return null;
    }

    public void setDoorWidth(String doorWidth) {
        if(doorWidth.isEmpty() || doorWidth == null || doorWidth == "")  this.doorWidth = 0;
        else this.doorWidth =Float.parseFloat(doorWidth);
    }

    public String getDoorHeight() {
        if(doorHeight != Float.MIN_VALUE)    return Float.toString(doorHeight);
        else return null;
    }

    public void setDoorHeight(String doorHeight) {
        if(doorHeight.isEmpty() || doorHeight == null || doorHeight == "")  this.doorHeight = 0;
        else this.doorHeight =Float.parseFloat(doorHeight);
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> element) {
        this.elements = element;
    }

    public String getIsWindow() {
        return isWindow;
    }

    public void setIsWindow(String window) {
        this.isWindow =  (window);
    }


    public String getWindowWidth() {
            if(windowWidth != Float.MIN_VALUE)    return Float.toString(windowWidth);
            else return null;
    }

    public void setWindowWidth(String windowWidth) {
        if(windowWidth.isEmpty() || windowWidth == null || windowWidth == "")  this.windowWidth = 0;
        else this.windowWidth =Float.parseFloat(windowWidth);
    }

    public String getWindowHeight(){
        if(windowHeight != Float.MIN_VALUE)    return Float.toString(windowHeight);
    else return null;
    }

    public void setWindowHeight(String windowHeight) {
        if(windowHeight.isEmpty() || windowHeight == null || windowHeight == "")  this.windowHeight = 0;
        else this.windowHeight =Float.parseFloat(windowHeight);
    }

    public String getWindowBreast() {
        if(windowBreast != Float.MIN_VALUE)    return Float.toString(windowBreast);
        else return null;
    }

    public void setWindowBreast(String windowBreast) {
        if(windowBreast.isEmpty() || windowBreast == null || windowBreast == "")  this.windowBreast = 0;
        else this.windowBreast =Float.parseFloat(windowBreast);
    }

    public String getHeight() {
        if(height != Float.MIN_VALUE)    return Float.toString(height);
        else return null;
    }

    public void setHeight(String height) {
        if(height.isEmpty() || height == null || height == "")  this.height = 0;
        else this.height =Float.parseFloat(height);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsDoor() {
        return isDoor;
    }

    public void setIsDoor(String door) {
        this.isDoor = door;
    }
}
