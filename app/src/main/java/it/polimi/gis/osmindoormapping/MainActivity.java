package it.polimi.gis.osmindoormapping;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS = 100;
    private static final int STORAGE_PERMISSIONS = 1;

    private boolean isCameraPermissionGranted = false;
    private boolean isLocationPermissionGranted = false;
    private boolean isStoragePermissionGranted = false;

    private Uri fileUri;

    static {
        //If you use OpenCV 2.*, use "opencv_java"
        System.loadLibrary("opencv_java3");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton newPanoFAB = (FloatingActionButton) findViewById(R.id.takePhotoFAB);
        if (newPanoFAB != null) {

            newPanoFAB.setOnClickListener(newPanoFABListener);
            //startActivity(new Intent(getApplicationContext(), CameraActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String permissionRequest = null;
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            isStoragePermissionGranted = false;
            permissionRequest = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            String[] permissionsRequestsArray = {permissionRequest};
            ActivityCompat.requestPermissions(MainActivity.this, permissionsRequestsArray, STORAGE_PERMISSIONS);
        } else {
            isStoragePermissionGranted = true;
            loadGallery();
        }
    }

    private void loadGallery() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "OSMIndoorMapping");

        if (mediaStorageDir.exists()) {

            String directoryPath = mediaStorageDir.getPath();
            ListView imagesListView = (ListView) findViewById(R.id.listview_gallery);
            if (imagesListView != null) {
                imagesListView.setAdapter(new GalleryAdapter(this, directoryPath));
            }
        }
    }

    View.OnClickListener newPanoFABListener = new View.OnClickListener() {
        public void onClick(View v) {

            ArrayList<String> permissionsRequests = new ArrayList<>();

            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                isCameraPermissionGranted = false;
                permissionsRequests.add(Manifest.permission.CAMERA);
            } else {
                isCameraPermissionGranted = true;
            }

            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                isLocationPermissionGranted = false;
                permissionsRequests.add(Manifest.permission.ACCESS_FINE_LOCATION);
            } else {
                isLocationPermissionGranted = true;
            }

            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                isStoragePermissionGranted = false;
                permissionsRequests.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } else {
                isStoragePermissionGranted = true;
            }

            if (permissionsRequests.size() > 0) {
                String[] permissionsRequestsArray = permissionsRequests.toArray(new String[permissionsRequests.size()]);
                ActivityCompat.requestPermissions(MainActivity.this, permissionsRequestsArray, MY_PERMISSIONS);
            } else {

                captureAndSaveImage();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS: {
                if (grantResults.length > 0) {

                    boolean arePermissionsGranted = true;
                    for (int permission : grantResults) {
                        if (permission != PackageManager.PERMISSION_GRANTED)
                            arePermissionsGranted = false;
                    }

                    if (arePermissionsGranted) {
                        captureAndSaveImage();
                    } else {
                        Toast.makeText(getApplicationContext(), "Tutti i permessi richiesti sono necessari al funzionamento dell'applicazione.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Tutti i permessi richiesti sono necessari al funzionamento dell'applicazione.", Toast.LENGTH_LONG).show();
                }
                break;
            }

            case STORAGE_PERMISSIONS: {

                if (grantResults.length > 0) {

                    boolean arePermissionsGranted = true;
                    for (int permission : grantResults) {
                        if (permission != PackageManager.PERMISSION_GRANTED)
                            arePermissionsGranted = false;
                    }
                    if (arePermissionsGranted) {
                        loadGallery();
                    } else {
                        finish();
                        Toast.makeText(getApplicationContext(), "Senza questo permesso non puoi visualizzare alla galleria.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    finish();
                    Toast.makeText(getApplicationContext(), "Senza questo permesso non puoi visualizzare alla galleria.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private void captureAndSaveImage() {

        fileUri = getOutputMediaFileUri(); // create a file to save the image
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    /**
     * Create a file Uri for saving an image or video
     */
    private Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "OSMIndoorMapping");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("OSMIndoorMapping", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                System.out.println(fileUri.getPath());

                if (fileUri != null) {

                    galleryAddPic(fileUri.getPath());

                    Intent sendImage = new Intent(this, ImagePreviewActivity.class);
                    sendImage.putExtra("ImageUri", fileUri);
                    startActivity(sendImage);
                }

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
                Toast.makeText(this, "Operazione annullata", Toast.LENGTH_LONG).show();
            } else {
                // Image capture failed, advise user
            }
        }

    }

    private void galleryAddPic(String fileName) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(fileName);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
