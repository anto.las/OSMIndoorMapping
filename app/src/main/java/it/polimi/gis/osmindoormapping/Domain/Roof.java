package it.polimi.gis.osmindoormapping.Domain;

public class Roof{
    public enum Shape {flat, pitched, hipped};
    private  Shape type = Shape.flat;
    private String style= "";
    private String colour = "";

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Shape getType() {
        return type;
    }

    public void setType(Shape type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    private String material;
}
