package it.polimi.gis.osmindoormapping.Domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Claudio on 21/06/2016.
 */
public class Level {
    private List<Element> elements = new ArrayList();
    private Element base =new Element();
    private String name = "";
    private String usage = "";
    private float height = Float.MIN_VALUE ;
    private short level = Short.MIN_VALUE ;

    public String getLevel() {
    if(level != Short.MIN_VALUE)    return Short.toString(level);
    else return null;
    }

    public void setLevel(String level) {
    if(level.isEmpty() || level == null || level == "")  this.level = 0;
    else this.level =Short.parseShort(level);
    }

    public Element getBase() {
        return base;
    }

    public void setBase(Element base) {
        this.base = base;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getHeight() {
        if(height != Float.MIN_VALUE)    return Float.toString(height);
        else return null;
    }

    public void setHeight(String height) {
        if(height.isEmpty() || height == null || height == "")  this.height = 0;
        else this.height =Float.parseFloat(height);
    }

}
