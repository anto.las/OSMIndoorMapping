package it.polimi.gis.osmindoormapping.Domain;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Claudio on 21/06/2016.
 */

public class Element {


    private int id;
    private Date timestamp = Calendar.getInstance().getTime();
    private long uid=0;
    private String user="";
    private boolean visible= true;
    private int version='1';
    private long changeset = 0;
    public enum Type {
        way,
        relation,
        node,
        key
    }
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getChangeset() {
        return changeset;
    }

    public void setChangeset(long changeset) {
        this.changeset = changeset;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Element(int globalID) {
        id = globalID;
    }
    public Element() {}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    Point position = new Point();
    public float height = 0;
    public float width =0;

    public String getWidth() {
        return Float.toString(width);
    }

    public void setWidth(String width) {
        if(width != null && width != "" && !width.isEmpty() && width !=" ") this.width = Float.parseFloat(width);
        else this.width = 0;
    }

    public String getHeight() {
        return Float.toString(height);
    }

    public void setHeight(String length) {
        if(length != null && length != "" && !length.isEmpty() && length !=" ")
            this.height = Float.parseFloat(length);
        else this.height = 0;
    }

    private List<String> tags = new ArrayList<>();
}
/*
class Node extends Element{



}
class Way extends Element{

    private final List<Node> nodes = new ArrayList<Node>();
}
class Relation extends Element {


    List<Element> parts = new ArrayList<>();

    IDs,
    way IDs
    and relation
    IDs in
    this relation.
            members
}*/