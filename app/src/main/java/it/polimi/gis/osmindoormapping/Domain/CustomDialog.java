package it.polimi.gis.osmindoormapping.Domain;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import it.polimi.gis.osmindoormapping.ImageActivity;
import it.polimi.gis.osmindoormapping.R;
import it.polimi.gis.osmindoormapping.databinding.CustomDialogBinding;

public class CustomDialog extends DialogFragment {
    CustomDialogBinding binding;

    public Building build;
    BuildingPart buildPart;
    Level level;
    int mNum = 4;
    private int mStackLevel = 0;
    private View oldTab;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static CustomDialog newInstance() {
        CustomDialog f = new CustomDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", 0);

        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = 4;

        if (!this.getTag().isEmpty()) {
            String a[] = getTag().split("-");
            switch (a[0]) {
                case "build":
                    build = ImageActivity.mainBuild;
                    break;
                case "buildpart":
                    buildPart = ImageActivity.buildPartArray.get(Integer.parseInt(a[1]));

                     break;
                case "level":
                    level = ImageActivity.mainLevel;
                    break;
                default:
                    build = new Building();
                    buildPart = new BuildingPart();
                    level = new Level();
                    break;
            }
        } else {
            build = new Building();
            buildPart = new BuildingPart();
            level = new Level();
        }

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        style = DialogFragment.STYLE_NORMAL;
        theme = android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth;

        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.custom_dialog, container, false);
        binding.setBuild(build);
        binding.setBuildpart(buildPart);
        binding.setFloor(level);



        final View v = binding.getRoot();

        Spinner spn  =(Spinner) v.findViewById(R.id.typeSp);
        spn.setAdapter(new ArrayAdapter<BuildingPart.BuildPartType>(this.getActivity(), android.R.layout.simple_list_item_1, BuildingPart.BuildPartType.values()));
        if(buildPart.getBuiltpartType() != null) spn.setSelection(buildPart.getBuiltpartType().ordinal());
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position) != null)
               buildPart.setBuiltpartType((BuildingPart.BuildPartType)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spn2  =(Spinner) v.findViewById(R.id.conditionSp);
        spn2.setAdapter(new ArrayAdapter<Building.BuildCondition>(this.getActivity(), android.R.layout.simple_list_item_1, Building.BuildCondition.values()));
//        if(build.getCondition() != null) spn.setSelection(build.getCondition().ordinal());
        spn2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position) != null)
                build.setCondition((Building.BuildCondition)(parent.getItemAtPosition(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button bt = (Button) v.findViewById(R.id.okButton);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Spinner spn  =(Spinner) binding.getRoot().findViewById(R.id.spinner);
                switch (spn.getSelectedItemPosition()) {
                 case 1:*/

                switch (getTag()) {
                    case "startB":
                        if (ImageActivity.mainBuild == null) {
                            build.getBase().setId(ImageActivity.globalID);
                            ImageActivity.globalID++;
                        }
                        ImageActivity.mainBuild = build;
                        break;
                    case "startL":
                        if (ImageActivity.mainLevel == null) {
                            level.getBase().setId(ImageActivity.globalID);
                            ImageActivity.globalID++;
                        }
                        ImageActivity.mainLevel = level;
                        break;
                    case "":
                        buildPart.getBase().setId(ImageActivity.globalID);
                        ImageActivity.globalID++;
                        ImageActivity.buildPartArray.add(buildPart);
                        ImageActivity.buildPartNames.add(buildPart.getName());
                        break;

                    default:
                        String a[] = getTag().split("-");
                        ImageActivity.buildPartArray.remove(Integer.parseInt(a[1]));
                        ImageActivity.buildPartArray.add(Integer.parseInt(a[1]), buildPart);
                        break;
                }

                ImageActivity.isSaved = true;
                dismiss();
            }
        });
        Button bt2 = (Button) v.findViewById(R.id.dismissButton);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
            }
        });
        switch (getTag()) {
            case "startB":
                ((TextView) v.findViewById(R.id.dialogTitle)).setText("BUILDING");
                v.findViewById(R.id.l1).setVisibility(View.VISIBLE);
                break;
            case "startL":
                ((TextView) v.findViewById(R.id.dialogTitle)).setText("LEVEL");
                v.findViewById(R.id.l3).setVisibility(View.VISIBLE);
                break;
            default:
                ((TextView) v.findViewById(R.id.dialogTitle)).setText("BUILDING PART");
                v.findViewById(R.id.l2).setVisibility(View.VISIBLE);
                break;
        }

     /*   final Spinner sp  =(Spinner) v.findViewById(R.id.spinner);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        if(oldTab != null)oldTab.setVisibility(View.GONE);
                        v.findViewById(R.id.l1).setVisibility(View.VISIBLE);
                        oldTab =  v.findViewById(R.id.l1);
                        break;
                    case 0:
                        if(oldTab != null)oldTab.setVisibility(View.GONE);
                        v.findViewById(R.id.l2).setVisibility(View.VISIBLE);
                        oldTab =  v.findViewById(R.id.l2);
                        break;
                    case 2:
                        if(oldTab != null)oldTab.setVisibility(View.GONE);
                        v.findViewById(R.id.l3).setVisibility(View.VISIBLE);
                        oldTab =  v.findViewById(R.id.l3);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        return v;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        if (this.getTag().isEmpty() || this.getTag()=="startB" ) {
            final Activity activity = getActivity();
            if (activity instanceof DialogInterface.OnDismissListener) {
                ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
            }
        }
    }
}