package it.polimi.gis.osmindoormapping;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.Arrays;

/**
 * Created by Antonio on 21/06/2016.
 */
public class ImageProcessor {

    public static Mat remapCoordinate(Mat sourceImage, Point[] srcPoints, Point[] dstPoints) {

        if (srcPoints.length != 3 || dstPoints.length != 3 || srcPoints.length != dstPoints.length)
            return null;

        Mat remappedImage = new Mat(sourceImage.size(), sourceImage.type());

        MatOfPoint2f srcPointsMat = new MatOfPoint2f();
        srcPointsMat.fromArray(srcPoints);
        MatOfPoint2f dstPointsMat = new MatOfPoint2f();
        dstPointsMat.fromArray(dstPoints);

        Mat trasformation = Imgproc.getAffineTransform(srcPointsMat,dstPointsMat);
        Imgproc.warpAffine(sourceImage, remappedImage, trasformation, remappedImage.size());

        return remappedImage;
    }

    public static Bitmap findOptimalSize(String path, int screenWidth) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        int sampleSize = 1;

        while (screenWidth < options.outWidth / sampleSize) {

            sampleSize++;
        }
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static Point getRealCoordinates(Point relativeCoordinates, Point viewSize, Point previewImageSize, Point realImageSize) {


        //Coordinates relative to the small image used for the preview with respect to the view size and view coordinates
        int x = (int) ((relativeCoordinates.x * previewImageSize.x) / viewSize.x);
        int y = (int) ((relativeCoordinates.y * previewImageSize.y) / viewSize.y);
        //Cordinates relative to the original image with respect to the preview image
        x = (int) ((x * realImageSize.x) / previewImageSize.x);
        y = (int) ((y * realImageSize.y) / previewImageSize.y);

        return new Point(x, y);
    }

    public static Point getRelativeCoordinates(Point realCoordinates, Point viewSize, Point previewImageSize, Point realImageSize) {

        //Coordinates relative to the small image used for the preview with respect to the real image size and real image coordinates
        int x = (int) ((realCoordinates.x * previewImageSize.x) / realImageSize.x);
        int y = (int) ((realCoordinates.y * previewImageSize.y) / realImageSize.y);
        //Cordinates relative to the original image with respect to the preview image
        x = (int) ((x * viewSize.x) / previewImageSize.x);
        y = (int) ((y * viewSize.y) / previewImageSize.y);

        return new Point(x, y);
    }

    public static Mat adjustPerspective(Mat srcImage, Point[] corners) {

        int minX = (int) Math.min(corners[0].x, corners[2].x);
        int minY = (int) Math.min(corners[0].y, corners[1].y);
        int maxX = (int) Math.max(corners[1].x, corners[3].x);
        int maxY = (int) Math.max(corners[2].y, corners[3].y);

        Point[] dstPoints = new Point[4];
        //tl
        dstPoints[0] = new Point(minX, minY);
        //tr
        dstPoints[1] = new Point(maxX, minY);
        //bl
        dstPoints[2] = new Point(minX, maxY);
        //br
        dstPoints[3] = new Point(maxX, maxY);

        Mat remappedImage = new Mat(srcImage.size(), srcImage.type());
        Mat srcPointsMat = Converters.vector_Point2f_to_Mat(Arrays.asList(corners));
        Mat dstPointsMat = Converters.vector_Point2f_to_Mat(Arrays.asList(dstPoints));

        Mat trasformation = Imgproc.getPerspectiveTransform(srcPointsMat, dstPointsMat);
        Imgproc.warpPerspective(srcImage, remappedImage, trasformation, remappedImage.size());

        return remappedImage;
    }
}
