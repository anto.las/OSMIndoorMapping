package it.polimi.gis.osmindoormapping.Domain;

import android.media.Image;

public class Facade{

    String color;

     public String getColor() {
         return color;
     }

     public void setColor(String colour) {
         this.color = colour;
     }

     public Image getImage() {
         return image;
     }

     public void setImage(Image image) {
         this.image = image;
     }

     Image image;
}
