package it.polimi.gis.osmindoormapping;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.gis.osmindoormapping.Domain.Building;
import it.polimi.gis.osmindoormapping.Domain.BuildingPart;
import it.polimi.gis.osmindoormapping.Domain.CustomDialog;
import it.polimi.gis.osmindoormapping.Domain.Element;
import it.polimi.gis.osmindoormapping.Domain.ImageDraw;
import it.polimi.gis.osmindoormapping.Domain.Level;

public class ImageActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {

    private ImageDraw lastImage;
    private List<ImageDraw> images = new ArrayList<>();
    private String filename = "";
    public static List<BuildingPart> buildPartArray = new ArrayList<>();
    public static List<String> buildPartNames = new ArrayList<>();
    public static Level mainLevel = null;
    public static Building mainBuild = null;
    public static int globalID = 0;
    public static boolean isSaved = false;



   /* final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public void onLongPress(MotionEvent e) {

        touchX =  e.getX();
         touchY= e.getY();

            CustomDialog dialog = new CustomDialog();
            dialog.show(getFragmentManager(), "");
        }
    });*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity);

        Uri imageUri = getIntent().getParcelableExtra("ImageUri");
        filename = imageUri.getPath().substring(0, imageUri.getPath().lastIndexOf('.'));
        ImageView view = (ImageView) findViewById(R.id.imageContainer);

        if (imageUri != null) {

            Point screenSize = new Point();
            getWindowManager().getDefaultDisplay().getSize(screenSize);
            Bitmap image = ImageProcessor.findOptimalSize(imageUri.getPath(), screenSize.x);
            if (view != null) {

                view.setImageBitmap(image);
                view.setAdjustViewBounds(true);
                view.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        return true;
                    }
                });
                FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
                ImageDraw view2 = new ImageDraw(getApplicationContext());
                view2.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                view2.setPadding(0, 0, 0, 0);
                view2.bringToFront();
                frameLayout.addView(view2);
                lastImage = view2;

                File file = new File(filename + ".xml");
                if (file.exists()) {
                    readXML(file);
                    List<Point> path = new ArrayList<>();
                    for (Element el : mainBuild.getElements()) {
                        path.add(el.getPosition());
                    }
                    lastImage.setPath(path);
                    lastImage.setNotBuild(false);
                    lastImage.saveCanvas();
                    for (BuildingPart bp : buildPartArray) {
                        ImageDraw view3 = new ImageDraw(getApplicationContext());
                        view3.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        view3.setPadding(0, 0, 0, 0);
                        view3.bringToFront();
                        frameLayout.addView(view3);
                        lastImage = view3;
                        path = new ArrayList<>();
                        for (Element el : bp.getElements()) {
                            path.add(el.getPosition());
                        }
                        lastImage.setPath(path);
                        lastImage.setName(bp.getName());
                        lastImage.saveCanvas();
                    }
                } else {
                    Toast.makeText(this.getApplicationContext(), "Insert the Building at first",
                            Toast.LENGTH_LONG).show();
                }
                Spinner spinner = (Spinner) findViewById(R.id.mainspinner);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, buildPartNames);
                spinner.setAdapter(adapter);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onDismiss(final DialogInterface dialog) {

        if (isSaved) {
            if (mainLevel != null) {
                List<Element> temp = new ArrayList<>();
                for (Point pt : lastImage.getPoints()) {
                    Element el = new Element(globalID);
                    el.setPosition(pt);
                    globalID++;
                    temp.add(el);
                }
                images.add(lastImage);
                buildPartArray.get(buildPartArray.size() - 1).setElements(temp);
                lastImage.setName(buildPartArray.get(buildPartArray.size() - 1).getName());
                lastImage.saveCanvas();
            } else {
                List<Element> temp = new ArrayList<>();
                for (Point pt : lastImage.getPoints()) {
                    Element el = new Element(globalID);
                    el.setPosition(pt);
                    globalID++;
                    temp.add(el);
                }
                mainBuild.setElements(temp);
                lastImage.setNotBuild(false);
                lastImage.saveCanvas();

                lastImage.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        return true;
                    }
                });
                lastImage = null;
                new CustomDialog().show(getFragmentManager(), "startL");
                Toast.makeText(this.getApplicationContext(), "Insert building elements",
                        Toast.LENGTH_LONG).show();
            }
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            ImageDraw view2 = new ImageDraw(getApplicationContext());
            view2.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            view2.bringToFront();
            frameLayout.addView(view2);
            lastImage = view2;

            Spinner spinner = (Spinner) findViewById(R.id.mainspinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, buildPartNames);
            spinner.setAdapter(adapter);
        } else {
            lastImage.Clear();
        }
        isSaved = false;


    }

    public void closeElement(View view) {
        CustomDialog dialog = new CustomDialog();
        if (mainBuild == null) {
            dialog.show(getFragmentManager(), "startB");
        } else dialog.show(getFragmentManager(), "");

    }

    public void editPart(View view) {
        Spinner spinner = (Spinner) findViewById(R.id.mainspinner);

        String tag = "buildpart-" + spinner.getSelectedItemPosition();
        CustomDialog dialog = new CustomDialog();
        dialog.show(getFragmentManager(), tag);

    }

    public void deletePart(View view) {
        Spinner spinner = (Spinner) findViewById(R.id.mainspinner);
        buildPartArray.remove(spinner.getSelectedItemPosition());
        buildPartNames.remove(spinner.getSelectedItemPosition());
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.removeView(images.get(spinner.getSelectedItemPosition()));
        images.remove(spinner.getSelectedItemPosition());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, buildPartNames);
        spinner.setAdapter(adapter);
    }

    public void writeXML(View view) {


        try {

            FileWriter output = new FileWriter(new File(filename + ".xml"), false);
            output.write("<osm version='0.1' generator='OSMIT' > \n");
            for (BuildingPart way : buildPartArray) {

                for (Element el : way.getElements()) {
                    output.write(" <node id='" + el.getId() + "' lat='" + el.getPosition().y + "' lon='" + el.getPosition().x + "' version='" + el.getVersion() + "' changeset='" + el.getChangeset() + "' user='' uid='' visible='" + el.isVisible() + "' timestamp='" + el.getTimestamp() + "'/>\n");
                }
            }
            for (Element el : mainBuild.getElements()) {
                output.write(" <node id='" + el.getId() + "' lat='" + el.getPosition().y + "' lon='" + el.getPosition().x + "' version='" + el.getVersion() + "' changeset='" + el.getChangeset() + "' user='' uid='' visible='" + el.isVisible() + "' timestamp='" + el.getTimestamp() + "'/>\n");
            }

            output.write("<relation id='" + mainBuild.getBase().getId() + "' version='" + mainBuild.getBase().getVersion() + "' changeset='" + mainBuild.getBase().getChangeset() + "' user='' uid='' visible='" + mainBuild.getBase().isVisible() + "' timestamp='" + mainBuild.getBase().getTimestamp() + "'>\n");
            output.write("  <member type='relation' ref='" + mainLevel.getBase().getId() + "' role='level_" + mainLevel.getLevel() + "' />\n");
            for (Element el : mainBuild.getElements()) {
                output.write("  <nd ref='" + el.getId() + "' />\n");
            }
            output.write("  <tag k='building' v='yes' />\n");
            output.write("  <tag k='building:architecture' v='" + mainBuild.getArchitetture() + "' />\n");
            output.write("  <tag k='building:buildyear' v='" + mainBuild.getBuildYear() + "' />\n");
            output.write("  <tag k='building:cladding' v='" + mainBuild.getCladding() + "' />\n");
            output.write("  <tag k='building:condition' v='" + mainBuild.getCondition().toString() + "' />\n");
            output.write("  <tag k='building:facade:colour' v='" + mainBuild.getFacade().getColor() + "' />\n");
            output.write("  <tag k='building:levels' v='" + mainBuild.getLevels() + "' />\n");
            output.write("  <tag k='building:roof:colour' v='" + mainBuild.getRoof().getColour() + "' />\n");
            output.write("  <tag k='building:roof:material' v='" + mainBuild.getRoof().getMaterial() + "' />\n");
            output.write("  <tag k='building:roof:shape' v='" + mainBuild.getRoof().getType() + "' />\n");
            if(mainBuild.getHeight() != null) output.write("  <tag k='height' v='" + mainBuild.getHeight() + "' />\n");
            output.write("  <tag k='name' v='" + mainBuild.getName() + "' />\n");
            output.write("  <tag k='type' v='building' />\n");
            output.write("</relation>\n");

            output.write("<relation id='" + mainLevel.getBase().getId() + "' timestamp='" + mainLevel.getBase().getTimestamp() + "' uid='' user='' visible='" + mainLevel.getBase().isVisible() + "' version='" + mainLevel.getBase().getVersion() + "' changeset='" + mainLevel.getBase().getChangeset() + "'>\n");
            for (BuildingPart bp : buildPartArray) {
                output.write("  <member type='way' ref='" + bp.getBase().getId() + "' role='buildingpart' />\n");
            }
            output.write("  <tag k='height' v='" + mainLevel.getHeight() + "' />\n");
            output.write("  <tag k='level' v='" + mainLevel.getLevel() + "' />\n");
            output.write("  <tag k='level:usage' v='" + mainLevel.getUsage() + "' />\n");
            output.write("  <tag k='name' v='" + mainLevel.getName() + "' />\n");
            output.write("  <tag k='type' v='level' />\n");
            output.write("</relation>\n");

            for (BuildingPart way : buildPartArray) {
                output.write("<way id='" + way.getBase().getId() + "' lat='" + way.getBase().getPosition().y + "' lon='" + way.getBase().getPosition().x + "' version='" + way.getBase().getVersion() + "' changeset='" + way.getBase().getChangeset() + "' user='' uid='' visible='" + way.getBase().isVisible() + "' timestamp='" + way.getBase().getTimestamp() + "'>\n");
                for (Element el : way.getElements()) {
                    output.write("  <nd ref='" + el.getId() + "' />\n");
                }

                if(way.getBuiltpartType() != null) output.write("  <tag k='buildingpart' v='" + way.getBuiltpartType().toString() + "' />\n");
                if(way.getHeight() != null) output.write("  <tag k='height' v='" + way.getHeight() + "' />\n");
                output.write("  <tag k='indoor' v='yes' />\n");
                output.write("  <tag k='name' v='" + way.getName() + "' />\n");

                if (way.getIsWindow() != "") {
                    output.write("  <tag k='window' v='" + way.getIsWindow() + "' />\n");
                    output.write("  <tag k='window.height' v='" + way.getWindowHeight() + "' />\n");
                    output.write("  <tag k='window.width' v='" + way.getWindowWidth() + "' />\n");
                    output.write("  <tag k='window.breast' v='" + way.getWindowBreast() + "' />\n");
                }
                if (way.getIsDoor() != "") {
                    output.write("  <tag k='door' v='" + way.getIsDoor() + "' />\n");
                    output.write("  <tag k='door.height' v='" + way.getDoorHeight() + "' />\n");
                    output.write("  <tag k='door.width' v='" + way.getDoorWidth() + "' />\n");
                }
                output.write("</way>\n");

            }
            output.write("</osm>\n");

            output.close();
            Toast.makeText(this.getApplicationContext(), "Mapping saved!",
                    Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this.getApplicationContext(), "Error can't save the mapping",
                    Toast.LENGTH_LONG).show();
        }

    }
    @Override
    public void onBackPressed() {

           buildPartArray = new ArrayList<>();
          buildPartNames = new ArrayList<>();
           mainLevel = null;
           mainBuild = null;
           globalID = 0;
           isSaved = false;
       finish();
    }

    public void readXML(File file) {

        String lastOpenTag = "";
        List<Element> elList = new ArrayList<>();
        try {

            FileReader input = new FileReader(file);
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();
            myParser.setInput(input);

            int event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                try {
                String tagname = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equals("way")) {
                            BuildingPart bp = new BuildingPart();
                            bp.getBase().setId(Integer.parseInt(myParser.getAttributeValue(null, "id")));
                            buildPartArray.add(bp);
                            lastOpenTag = "way";
                        }
                        else if (tagname.equals("relation")) {
                            if (mainBuild == null) {
                                mainBuild = new Building();
                                mainBuild.getBase().setId(Integer.parseInt(myParser.getAttributeValue(null, "id")));
                                lastOpenTag = "build";

                            } else {
                                mainLevel = new Level();
                                mainLevel.getBase().setId(Integer.parseInt(myParser.getAttributeValue(null, "id")));
                                lastOpenTag = "level";
                            }
                        } else if (tagname.equals("tag")) {
                            if (lastOpenTag.equals("way")) {
                                switch (myParser.getAttributeValue(null, "k")) {
                                    case "name":
                                        buildPartArray.get(buildPartArray.size() - 1).setName((myParser.getAttributeValue(null, "v")));
                                        buildPartNames.add((myParser.getAttributeValue(null, "v")));
                                        break;
                                    case "height":
                                        buildPartArray.get(buildPartArray.size() - 1).setHeight(myParser.getAttributeValue(null, "v"));
                                        break;
                                    case "buildingpart":
                                        buildPartArray.get(buildPartArray.size() - 1).setBuiltpartType(BuildingPart.BuildPartType.valueOf(myParser.getAttributeValue(null, "v")));
                                        break;
                                }
                            } else if (lastOpenTag.equals("level")) {
                                switch (myParser.getAttributeValue(null, "k")) {
                                    case "name":
                                        mainLevel.setName((myParser.getAttributeValue(null, "v")));
                                        break;
                                    case "height":
                                        mainLevel.setHeight(myParser.getAttributeValue(null, "v"));
                                        break;
                                }

                            } else if (lastOpenTag.equals("build")) {
                                switch (myParser.getAttributeValue(null, "k")) {
                                    case "name":
                                        mainBuild.setName((myParser.getAttributeValue(null, "v")));
                                        break;
                                    case "height":
                                        mainBuild.setHeight(myParser.getAttributeValue(null, "v"));
                                        break;
                                }
                            }
                        }
                        else if (tagname.equals("nd")) {
                            if (lastOpenTag.equals("way")) {
                                String a = myParser.getAttributeValue(null, "ref");
                                for (Element el : elList) {
                                    if(Integer.toString(el.getId()).equals(a))
                                        buildPartArray.get(buildPartArray.size()-1).getElements().add(el);
                                }
                            } else if (lastOpenTag == "build") {
                                String a = myParser.getAttributeValue(null, "ref");
                                for (Element el : elList) {
                                    if(Integer.toString(el.getId()).equals(a))
                                        mainBuild.getElements().add(el);
                                }
                            }
                        } else if (tagname.equals("node")) {
                            Element el = new Element();
                            el.setId(Integer.parseInt(myParser.getAttributeValue(null, "id")));
                            el.setPosition(new Point(Integer.parseInt(myParser.getAttributeValue(null, "lon")), Integer.parseInt(myParser.getAttributeValue(null, "lat"))));
                            elList.add(el);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
                event = myParser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}